<?php 
/*
 * Everything here is obvious
 * 
 */
class UserController extends Controller {
    public function login() {
        if(!isset($_SESSION["loggedInUserID"])) {
            if($_POST["uname"]=="") {
                return -1;
            }
            $r = $this->getUser($_POST["uname"],$_POST["pwd"]);
            if($r!==false) {
                $_SESSION["loggedInUserID"] = $r[0]["usr_id"];
                return $_SESSION["loggedInUserID"];
            }
            return false;
        }
        return $_SESSION["loggedInUserID"];
    }
    
    public function getUser($uname,$pwd) {
        $r = $this->model->login($uname,$pwd);
		if($r!==false && count($r)>0) {
			return $r;
		}
		else {
			return false;
		}
	}
	
	public function logout() {
		unset($_SESSION["loggedInUserID"]);
		return true;
	}
}
?>