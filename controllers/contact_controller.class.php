<?php 
class ContactController extends Controller {
    private $uid;
    
    function __construct($model) {
        parent::__construct($model);
        $this->uid = $_SESSION["loggedInUserID"];
    }
    
	function insert() {
	    $phones = (isset($_POST["numbers"]))?$_POST["numbers"]:false;
	    $validationResult = $this->validateContactData($_POST["fname"], $_POST["lname"], $phones);
	    if($validationResult["frmIsValid"]) {
		    return $this->model->addContact($_POST["cid"], $_POST["fname"], $_POST["lname"], $phones, $this->uid);
	    }
	    else {
	        return $validationResult;
	    }
	}
	
	function get() {
		$contacts = $this->model->getContacts($this->uid);
		if($contacts && count($contacts)>0) {
		    return $this->joinContactPhoneNumbers($contacts);
		}
		return array();
	}
	
	function filter() {
		$contacts = $this->model->filterContacts($_POST["kw"],$this->uid);
		if($contacts && count($contacts)>0) {
		    return $this->joinContactPhoneNumbers($contacts);
		}
		return array();
	}
	
	function delete($id) {
		return $this->model->deleteContact($id,$this->uid);
	}
		
	function update() {
		return $this->model->updateContact($_POST["cid"],$_POST["fname"],$_POST["lname"],$_POST["numbers"],$this->uid);
	}
	
	// Private methods 
	
	/*
	 * Join phone numbers list for each contact (current array contains result of left join between contact and phone: one record for each phone number)
	 * 
	 * Argument : an array of contact records (important: recordset is assumed sorted)
	 * 
	 * Returns the same array of contacts but with an extra array of phone numbers for each contact, eliminating other duplicate contact info
	 * 
	 */
	private function joinContactPhoneNumbers($contacts) {
	    $joinFixedContacts = array();
	    $currentCtcId = $contacts[0]["ctc_id"];
	    $currentCtcRecord = array_slice($contacts[0],0,4);
	    $currentCtcNumbers = array();
	    for($i=0; $i<count($contacts); $i++) {
	        if($currentCtcId != $contacts[$i]["ctc_id"]) {
	            $currentCtcId = $contacts[$i]["ctc_id"];
	            $currentCtcRecord = array_slice($contacts[$i],0,4);
	            $currentCtcNumbers = array();
	        }
	        $j = $i;
	        while($j<count($contacts) && $currentCtcId == $contacts[$j]["ctc_id"]) {
	            if($contacts[$j]["pho_id"]===NULL) {
	                $j++;
	                break;
	            }
	            $currentCtcNumbers[] = array_slice($contacts[$j],4,5);
	            $j++;
	        }
	        $i=$j-1;
	        $currentCtcRecord["ctc_phones"] = $currentCtcNumbers;
	        $joinFixedContacts[] = $currentCtcRecord;
	    }
	    return $joinFixedContacts;
	}
	
	/*
	 * 
	 * Validates the contact form fields
	 * See JS validation source code for comments on the data patterns assumptions
	 * 
	 * TODO: This does not belong in here: make it part of validation class ...
	 * 
	 */
	
	private function validateContactData($fname,$lname,$phones) {
	    $frmIsValid = true;
	    $frmErrors = array("name"=>false,"phones"=>array());
	    if(preg_match("/[a-z]/i", $fname)===0 && preg_match("/[a-z]/i", $lname)===0) {
	        $frmIsValid = false;
	        $frmErrors["name"] = true;
	    }
	    for($i=0;$i<count($phones); $i++) {
	        if(((isset($phones[$i]["number"]) && $phones[$i]["number"]!="" && preg_match("/^\d{6,15}$/", $this->sanitizePhone($phones[$i]["number"]))===0) || preg_match("/^\d{0,5}$/", $this->sanitizePhone($phones[$i]["extension"]))===0)) {
	            $frmIsValid = false;
	            $frmErrors["phones"][] = $i;
	        }
	    }
	    return array("frmIsValid"=>$frmIsValid,"frmErrors"=>$frmErrors);
	}
	
	
	private function sanitizePhone($phone) {
	    return str_replace(array('+','(',')','-','.',' '), '', $phone);
	}
}
?>