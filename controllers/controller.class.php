<?php 
// Base (and very basic!) Controller
class Controller {
	protected $model;

	function __construct($model) {
		$this->model = new $model();
	}
}
?>