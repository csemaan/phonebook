A simple phonebook webapp. 

Concepts : RESTful Webservices, MVC architecture, OOP 
Programming languages: PHP, JavaScript
Markup & layout: HTML5, CSS (SASS)
Data tools: MySQL, JSON
APIs: jQuery