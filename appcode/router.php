<?php 
if(isset($_GET["url"])) {
    session_start();
    // Change this to reference a path that is preferably not within reach of the Web Server
    require_once("../db/db_credentials.php");
    $router = new Router($_GET["url"]);
    // Returns a JSON string
    print(json_encode($router->route()));
}

/*
 * Router that handles "URL" sent as query parameters via AJAX
 * 
 */
class Router { 
    private $url;  
    
	function __construct($url) {
	    $this->url = $url;
	    spl_autoload_register(array($this,'classAutoload'));
	}
	
	/*
		Analyzes the URL parameter sent by AJAX and branch the execution to the adequate controller, passing method name and parameters
	*/
	public function route() {
	    // We assume the following URL convention: 
	    // module/action/param1/param2/.../paramN
		$urlArray = explode("/",$this->url);
		$module = $urlArray[0];
		array_shift($urlArray);
		$action = $urlArray[0];
		array_shift($urlArray);
		$queryParams = $urlArray;
		
		$controllerName = ucfirst($module)."Controller";
		$modelName = ucfirst($module)."Model";
		// Instantiate controller
		$controllerInstance = new $controllerName($modelName,$action);
		// Invoque method
		if(method_exists($controllerName,$action)) {
			return call_user_func_array(array($controllerInstance,$action),$queryParams);
		}
		else {
			return false;	
		}
	}
	
	/*
		Replaces a class name by the file name in which the class is defined 
		Example : "ContactController" becomes "contact_controller"
	
	*/
	static private function classNameToFileName($className) {
		return strtolower(preg_replace("/(.)([A-Z])/","$1_$2",$className));
	}
	
	/*
		A very basic Autoload implementation
	*/
	private function classAutoload($className) {
		$fn = Router::classNameToFileName($className);
		if(file_exists("../controllers/".$fn.".class.php"))
			require_once("../controllers/".$fn.".class.php");
		else if(file_exists("../models/".$fn.".class.php"))
			require_once("../models/".$fn.".class.php");
		else if(file_exists("../lib/".$fn.".class.php"))
			require_once("../lib/".$fn.".class.php");
	}
}

?>