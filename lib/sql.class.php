<?php 
// MySQLi Abstraction Class
// TODO : error handling, refining of select queries result, etc.

class Sql {
	protected $db;
	protected $result;
	
	function connect() {
		$this->db = new mysqli(MY_HOST,MY_USER,MY_PASS,MY_DBNAME);
		$this->db->query("SET NAMES 'UTF8'");
	}
	
	function close() {
		$this->db->close();
	}
	
	function clean($v,$valueIsNumeric) {
		if($valueIsNumeric) return (double)$v;
		return $this->db->real_escape_string($v);
	}
	
	function doQuery($q) {
		$this->result = $this->db->query($q);
	}
	
	function select($q) {
		$this->doQuery($q);
		$resultArray = array();
		while($record = $this->result->fetch_assoc()) {
			$resultArray[] = $record;
		}
		return $resultArray;
	}
	
	function update($q) {
		$this->doQuery($q);
		return $this->db->insert_id;
	}
	
	function insert($q) {
		$this->doQuery($q);
		return $this->db->insert_id;
	}
	
	function delete($q) {
		$this->doQuery($q);
		return $this->db->affected_rows;
	}
}
?>