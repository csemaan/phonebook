/**
 * Phonebook client-side logic. 
 * TODO: Modularize; apply MVC pattern on this side of the code, etc.
 * 
 */
(function() {
	// Prepare for future enhancement: store contacts in Local Storage
	var contacts = new Array();
	var lastSavedContactId = 0;
	
	// Set all event handlers at DOM ready.
	$(function() {
		// Check login state at DOM load.
		login();
		
		// Static handlers
		$("#pb_new_btn").on("click",showNewContactForm);
		$("#pb_cancel_btn").on("click",cancelContactForm);
		$("#pb_addphone_btn").on("click",addPhoneRow);
		$("#pb_save_btn").on("click",saveContact);
		$("#pb_search").on("keyup",filterContacts);
		$("#pb_login_btn").on("click",login);
		$("#pb_logout_btn").on("click",logout);
		$("#pb_uname,#pb_pwd").on("keypress",function(e){if(e.which==13){login();}});
		
		// Delegated handlers
		$("#pb_main").on("click","i.delete_btn",deleteContact);
		$("#pb_main").on("click","i.edit_btn",fillContactForm);
		
		// Visual effects
		// Hover on contact list
		$("#pb_main").on("mouseenter","article",highlightContact);
		$("#pb_main").on("mouseleave","article",unhighlightContact);
		
		// Mark a contact (when clicked or newly added)
		$("#pb_main").on("click","article",markContact);
	});
	
	
	/* 
	 * User Management
	 * Handle user login/logout - No other user management in this prototype (TODO: new user, lost password, etc.)
	 */
	function login() {
		$.ajax({url:"appcode/router.php?url=user/login",type:"POST",data:{uname:$("#pb_uname").val(),pwd:$("#pb_pwd").val()},success:h_login});
	}
	
	function h_login(data,status,xhr) {
		if(data && data!=="false" && data!=-1) {
			$("#pb_login_frm").hide();
			$("#pb_login_frm .errormsg").hide();
			$("#pb_filter,#pb_main,#pb_logout").show();
			getContacts();
		}
		else {
			$("#pb_login_frm").slideDown(100);
			$("#pb_uname").focus();
			$("#pb_filter,#pb_main,#pb_logout").hide();
			if(data=="false") {
				$("#pb_login_frm .errormsg").show();
			}
		}
	}
	
	function logout() {
		$.ajax({url:"appcode/router.php?url=user/logout",type:"GET",success:h_logout});
	}
	
	function h_logout(data,status,xhr) {
		if(data) {
			$("#pb_login_frm").slideDown(100);
			$("#pb_uname").focus();
			$("#pb_filter,#pb_main,#pb_logout,#pb_new_frm").hide();
		}
	}
	
	/*
	 * One form is used for saving contacts (insert and update operations)
	 * 
	 */
	function setContactForm(id,fname,lname,numbers) {
		$("#pb_cid").val(id||'');
		$("#pb_firstname").val(fname||'');
		$("#pb_lastname").val(lname||'');
		// Clone the template for each phone number
		if(numbers) {
			var $phoneClone;
			for(var i=0; i<numbers.length; i++) {
				$phoneClone = $("#pb_new_frm .phone:last").clone();
				$(".number",$phoneClone).val(numbers[i].pho_number);
				if(numbers[i].pho_extension!=0) {
					$(".extension",$phoneClone).val(numbers[i].pho_extension);
				}
				$(".type",$phoneClone).val(numbers[i].pho_type);
				$("#pb_new_frm .phone:last").before($phoneClone);
			}
		}
		else {
			// If there are no numbers for this contact, remove all previously injected elements except the last one (which is our template)
			$("#pb_new_frm .phone").not(":last").each(function() {$(this).remove();});
		}
	}
	
	/*
	 * Reset contact form after save, closing, searching, etc.
	 * 
	 */
	function resetContactForm() {
		setContactForm(0,'','',null);
		$("#pb_new_frm .errormsg").hide();
		$("#pb_new_frm .phone:last .number,#pb_new_frm .phone:last .extension").val("");
	}
	
	/*
	 * New contact was pressed
	 * 
	 */
	function showNewContactForm(){
		resetContactForm();
		$("#pb_new_frm").slideDown(100);
		$("#pb_firstname").focus();
		markContact(); // toggle the marking of any contact in the list. 
	}

	/*
	 * Close contact form
	 * 
	 */
	function cancelContactForm() {
		resetContactForm();
		$("#pb_new_frm").slideUp(100);
	}
	
	/*
	 * Add new phone rows in the contact form (cloning DOM elements)
	 * 
	 */
	function addPhoneRow() {
		$phoneClone = $("#pb_new_frm .phone:last").clone();
		$("input",$phoneClone).val('');
		$("#pb_new_frm .phone:last").after($phoneClone);
		$(".number",$phoneClone).focus();
	}
	
	/*
	 * Validate the contact form and make an AJAX request to save the data server-side
	 * 
	 */
	function saveContact() {
		$(".errormsg","#pb_new_frm").hide();
		//if(validateContactInput()) {
			var cid = $("#pb_cid").val();
			var fname = $("#pb_firstname").val();
			var lname = $("#pb_lastname").val();
			// Create an array of numbers for multiple phones...
			var numbers = $("#pb_new_frm .phone").map(function() {
								if($(".number",$(this)).val()) { 
									return {"type":$(".type",$(this)).val(),"number":$(".number",$(this)).val().sanitizePhone(),"extension":$(".extension",$(this)).val().sanitizePhone()};
								}
							}).get();
			$.ajax({url:"appcode/router.php?url=contact/insert",type:"POST",data:{cid:cid,fname:fname,lname:lname,numbers:numbers},success:h_saveContact,dataType:"json"});
		//}
	}
	
	/*
	 * Handle the result of saving contact data, taking care of the server response in case of validation errors.
	 * 
	 */
	function h_saveContact(data,status,xhr) {
		if(data && data.frmErrors) {
			validateContactInput();
		}
		else {
			if(data=="0") {
				$(".errormsg","#pb_duplicate_msg").show();
			}
			else {
				cancelContactForm();
				getContacts();
				resetSearchInput();
				lastSavedContactId = data;
			}
		}
	}
	
	/*
	 * Get all contacts for current logged user.
	 * 
	 */
	function getContacts() {
		$.ajax({url:"appcode/router.php?url=contact/get",success:h_getContacts,dataType:"json"});
	}
	
	/*
	 * Store contacts data locally and call display.
	 * 
	 */
	function h_getContacts(data,status,xhr) {
		if(data) {
			contacts = data; // TODO: use local storage to manipulate contact data
			displayContacts();
		}
	}
	
	/*
	 * Remove all contact DOM elements except the template element
	 * 
	 */
	function cleanContactsDisplay() {
		$("#pb_main article").not(".templ_contact").each(function() {$(this).remove();});
	}
	
	// TODO: Can be used with contact data in local storage.
	/*
	 * Create the required DOM to diplay the contact list
	 * 
	 */
	function displayContacts() {
		cleanContactsDisplay();
		
		if(contacts.length===0) {
			$("#pb_main").hide();
		}
		else {
			$("#pb_main").show();
		}
		
		var $contactTemplClone,contact, phones, $phoneClone;
		
		for(var i=0; i<contacts.length;i++) {
			contact = contacts[i];
			$contactTemplClone = $("#pb_main .templ_contact").clone();
			$contactTemplClone.removeClass("templ_contact");
			$contactTemplClone.data("pos",i);
			$contactTemplClone.data("cid",contact.ctc_id);
			$(".name_listview",$contactTemplClone).text(contact.ctc_lastname + ((contact.ctc_lastname!="")?", ":"") + contact.ctc_firstname);
			
			if(contact.ctc_phones) {
				phones = contact.ctc_phones;
				
				for(var j=0; j<phones.length; j++) {
					$phoneClone = $(".phone_listview li:last",$contactTemplClone).clone();
					$phoneClone.removeClass("templ_phone");
					$(".type_listview",$phoneClone).text(phones[j].pho_type);
					$(".number_listview",$phoneClone).text(phones[j].pho_number);
					if(phones[j].pho_extension!=0) {
						$(".extension_listview",$phoneClone).text(" ext " + phones[j].pho_extension);
					}
					$(".phone_listview li:last",$contactTemplClone).before($phoneClone);
				}
			}
			$("#pb_main .templ_contact:last").before($contactTemplClone);
			if(contact.ctc_id==lastSavedContactId) $(".name_listview",$contactTemplClone).click(); // Mark it if newly saved or added.
		}
		$("#pb_main article:last").prev().addClass("last_article");
		displayCount();
	}
	
	function displayCount() {
		$("#list_footer").text(contacts.length + " item" + ((contacts.length>1)?"s":"") + ".");
	}
	
	/*
	 * Get filtered contact list (multiple keywords - OR operator)
	 * 
	 */
	function filterContacts(){
		cancelContactForm();
		if($(this).val().length==0)
			getContacts();
		else
			$.ajax({url:"appcode/router.php?url=contact/filter",type:"POST",data:{kw:$(this).val()},success:h_getContacts,dataType:"json"});
	}
	
	function markContact() {
		$("#pb_main article").removeClass("selected");
		$(this).closest("article").addClass("selected");
	}
	
	function highlightContact() {
		$(this).addClass("hover");
	}
	
	function unhighlightContact() {
		$(this).removeClass("hover");
	}
	
	/*
	 * Delete one contact
	 * 
	 */
	function deleteContact() {
		var $contact = $(this).closest("article"); 
		contacts.splice($contact.data("pos"),1);
		$.ajax({url:"appcode/router.php?url=contact/delete/"+$contact.data("cid"),success:displayContacts});
		lastSavedContactId = 0;
	}
	
	/*
	 * Fill contact form with currently selected contact for editing
	 * 
	 */
	function fillContactForm() {
		resetContactForm();
		var contact = contacts[$(this).closest("article").data("pos")];
		setContactForm(contact.ctc_id,contact.ctc_firstname,contact.ctc_lastname,contact.ctc_phones);
		$("#pb_firstname").focus();
		$("#pb_new_frm").slideDown(100);
	}
	
	/*
	 * Reset the search input textbox
	 * 
	 */
	function resetSearchInput() {
		$("#pb_search").val("");
	}
	
	
	/*
	 * Validation
	 */
	
	/*
	 * Sanitize the phone strings: we want to accept all usual characters (,),.,-,+ and whitespace.
	 * 
	 */
	String.prototype.sanitizePhone = function() {
		return this.replace(/[\+\(\)\-\.\s]/g, "");
	};
	
	/*
	 * Validate the name and the phone numbers and extensions of the contact form. 
	 * Some rules were arbitrarely set: like the name must contain at least one letter either in the firstname or the lastname
	 * Phone numbers are either empty (allowing users to add contact without numbers for future editing), or must contain a phone 
	 * number between 6 and 15 digits (for intl numbers). Extensions are limited to 5 digits.
	 * 
	 */
	function validateContactInput() {
		var $fname = $("#pb_firstname");
		var $lname = $("#pb_lastname");
		var $phones = $("#pb_new_frm .phone");
		
		var formIsValid = true;
		
		if($fname.val().search(/[a-z]/i)==-1 && $lname.val().search(/[a-z]/i)==-1) {
			$(".errormsg",$fname.closest("div.row")).show();
			formIsValid = false;
		}
		
		$phones.each(function() {
			if(($(".number",$(this)).val().sanitizePhone()!="" && $(".number",$(this)).val().sanitizePhone().search(/^\d{6,15}$/)==-1) || $(".extension",$(this)).val().sanitizePhone().search(/^\d{0,5}$/)==-1) {
				$(".errormsg",$(this)).show();
				formIsValid = false;
			}
		});
		
		return formIsValid;
	}
})();