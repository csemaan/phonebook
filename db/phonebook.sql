-- DB Strcuture & Test Data for Phonebook Web Application

-- Temporarely disable referential integrity checks.
SET FOREIGN_KEY_CHECKS=0;

CREATE DATABASE `phonebook`;
USE `phonebook`;


CREATE TABLE contact (
  ctc_id int(10) unsigned NOT NULL auto_increment,
  ctc_firstname varchar(25) collate utf8_unicode_ci default NULL,
  ctc_lastname varchar(25) collate utf8_unicode_ci NOT NULL,
  ctc_usr_id_fk mediumint(8) unsigned NOT NULL,
  PRIMARY KEY  (ctc_id),
  UNIQUE KEY ctc_firstname (ctc_firstname,ctc_lastname,ctc_usr_id_fk),
  KEY ctc_usr_id_fk (ctc_usr_id_fk)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO contact VALUES(82, 'Akira', 'Kurosawa', 2);
INSERT INTO contact VALUES(74, 'Edgar Allen', 'Poe', 1);
INSERT INTO contact VALUES(79, '�milie', 'Hague', 2);
INSERT INTO contact VALUES(73, 'Jorge Luis', 'Borges', 1);
INSERT INTO contact VALUES(78, 'Luis', 'Mareno', 2);
INSERT INTO contact VALUES(75, 'Marguerite', 'Duras', 1);
INSERT INTO contact VALUES(81, 'Otto', 'Schmidt', 2);
INSERT INTO contact VALUES(83, 'Pierre', 'Savard', 2);
INSERT INTO contact VALUES(80, 'Sindy', 'Lawrence', 2);
INSERT INTO contact VALUES(76, 'Thomas', 'Bernhard', 1);
INSERT INTO contact VALUES(77, 'Vladimir', 'Nabokov', 1);


CREATE TABLE phone (
  pho_id int(10) unsigned NOT NULL auto_increment,
  pho_number bigint(20) unsigned NOT NULL,
  pho_extension mediumint(8) unsigned NOT NULL,
  pho_type enum('mobile','home','office','other') collate utf8_unicode_ci NOT NULL default 'other',
  pho_ctc_id_fk int(10) unsigned NOT NULL,
  PRIMARY KEY  (pho_id),
  UNIQUE KEY pho_number (pho_number,pho_extension,pho_ctc_id_fk),
  KEY pho_ctc_id_fk (pho_ctc_id_fk)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO phone VALUES(84, 4383933989, 0, 'mobile', 58);
INSERT INTO phone VALUES(85, 51494841841, 0, 'home', 58);
INSERT INTO phone VALUES(88, 5149889999, 0, 'mobile', 61);
INSERT INTO phone VALUES(89, 453453453453, 0, 'mobile', 62);
INSERT INTO phone VALUES(90, 5653456456, 0, 'mobile', 63);
INSERT INTO phone VALUES(91, 51487458745, 585, 'mobile', 64);
INSERT INTO phone VALUES(92, 5144053976, 0, 'home', 65);
INSERT INTO phone VALUES(93, 4383933989, 0, 'mobile', 66);
INSERT INTO phone VALUES(94, 5144544530, 5463, 'office', 66);
INSERT INTO phone VALUES(102, 35657778, 1233, 'office', 67);
INSERT INTO phone VALUES(103, 2126627247684, 4546, 'office', 67);
INSERT INTO phone VALUES(104, 12345678, 0, 'mobile', 67);
INSERT INTO phone VALUES(106, 324454, 0, 'mobile', 68);
INSERT INTO phone VALUES(107, 234567890, 345, 'office', 68);
INSERT INTO phone VALUES(110, 23454546667, 4556, 'office', 70);
INSERT INTO phone VALUES(111, 5145467876, 0, 'mobile', 70);
INSERT INTO phone VALUES(112, 5144566574, 0, 'mobile', 71);
INSERT INTO phone VALUES(113, 5144325678, 4567, 'office', 71);
INSERT INTO phone VALUES(114, 45567890087, 0, 'office', 72);
INSERT INTO phone VALUES(117, 23476584095, 456, 'office', 69);
INSERT INTO phone VALUES(118, 23455789, 3455, 'office', 69);
INSERT INTO phone VALUES(119, 296615123456, 0, 'mobile', 73);
INSERT INTO phone VALUES(120, 16176354366, 0, 'home', 74);
INSERT INTO phone VALUES(121, 16177787817, 1234, 'office', 74);
INSERT INTO phone VALUES(122, 3325258965, 0, 'home', 75);
INSERT INTO phone VALUES(123, 3369854785, 0, 'mobile', 75);
INSERT INTO phone VALUES(124, 436765875874, 0, 'mobile', 76);
INSERT INTO phone VALUES(125, 12125875896, 0, 'home', 77);
INSERT INTO phone VALUES(126, 17189653256, 0, 'mobile', 77);
INSERT INTO phone VALUES(127, 17185214587, 8523, 'mobile', 77);
INSERT INTO phone VALUES(129, 3398653212, 785, 'office', 79);
INSERT INTO phone VALUES(130, 3350258745, 0, 'mobile', 79);
INSERT INTO phone VALUES(131, 9631254578, 0, 'home', 80);
INSERT INTO phone VALUES(132, 9638965896, 0, 'other', 80);
INSERT INTO phone VALUES(133, 6487478523, 1254, 'office', 81);
INSERT INTO phone VALUES(134, 8145845896, 854, 'office', 82);
INSERT INTO phone VALUES(135, 8156985235, 0, 'mobile', 82);
INSERT INTO phone VALUES(136, 8163256988, 0, 'home', 82);
INSERT INTO phone VALUES(137, 14189856325, 0, 'home', 83);
INSERT INTO phone VALUES(138, 14509874563, 0, 'other', 83);
INSERT INTO phone VALUES(139, 14508547859, 0, 'home', 78);


CREATE TABLE `user` (
  usr_id mediumint(8) unsigned NOT NULL auto_increment,
  usr_uname varchar(25) collate utf8_unicode_ci NOT NULL,
  usr_pwd char(128) collate utf8_unicode_ci NOT NULL,
  usr_pwdsalt char(23) collate utf8_unicode_ci NOT NULL,
  usr_firstname varchar(25) collate utf8_unicode_ci default NULL,
  usr_lastname varchar(25) collate utf8_unicode_ci default NULL,
  usr_email varchar(50) collate utf8_unicode_ci NOT NULL,
  usr_since datetime NOT NULL,
  PRIMARY KEY  (usr_id),
  UNIQUE KEY usr_uname (usr_uname)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO user VALUES(1, 'jdoe', 'fef6659ea15ab095a7d2b0ed6a4a2e671b1fbc98d0614e14cd10ed724dbdf7e5d54a5e5681f19110a73fe4f291ce7772187c62f09a8533f0bdfb5c7de6468963', '508364c91e0c41.94989226', 'John', 'Doe', 'jdoe@test.com', '2012-10-20 23:00:43');
INSERT INTO user VALUES(2, 'test', '5a4e7a1d476e165135621fece4bc059a53c5ebdd9228d5ba3dbe6db5632fab756691173352c0d74fd6659063cd74c3a24c3cf2c0b9c5ac073c372a6fd965b6dd', '5085dbbdeb0339.91147138', 'Monsieur', 'Teste', 'test@test.com', '2012-10-22 16:53:00');



ALTER TABLE `contact`
  ADD CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`ctc_usr_id_fk`) REFERENCES `user` (`usr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `phone`
  ADD CONSTRAINT `phone_ibfk_1` FOREIGN KEY (`pho_ctc_id_fk`) REFERENCES `contact` (`ctc_id`) ON DELETE CASCADE ON UPDATE CASCADE;

SET FOREIGN_KEY_CHECKS=1;
