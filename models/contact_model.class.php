<?php 
class ContactModel extends Sql {
    /*
     * Insert new contact and phone numbers in the DB.
     * All operations check for the usr_id which is provided by the controller from the session variable.
     */
	function addContact($cid,$fname,$lname,$numbers,$uid) {
		$this->connect();
		$cid = $this->clean($cid,true);
		if($cid>0) {
		    return $this->updateContact($cid,$fname,$lname,$numbers,$uid);
		}
		$fname = $this->clean($fname,false);
		$lname = $this->clean($lname,false);
		$uid = $this->clean($uid,true);
		$q1 = "INSERT INTO contact VALUES ($cid,'$fname','$lname',$uid)";
		$r1 = $this->insert($q1);
		// If we inserted the contact and we have phone numbers to add:
		if($r1 && $numbers) {
		    $multiInsert = "";
		    // Construct a multi-insert query.
		    foreach($numbers as $phone) {
		        $type = $this->clean($phone["type"],false);
		        $number = $this->clean($phone["number"],true);
		        $ext = $this->clean($phone["extension"],true);
		        $multiInsert .= "(0,$number,$ext,'$type',$r1),";
		    }
		    $q2 = "INSERT INTO phone VALUES ".rtrim($multiInsert,",");
		    $r2 = $this->insert($q2);
		}
		$this->close();
		return $r1;
	}
	
	/*
	 * 
	 * Update a contact record, updating all phone numbers at the same time.
	 * 
	 */
	function updateContact($cid,$fname,$lname,$numbers,$uid) {
	    $this->connect();
	    $cid = $this->clean($cid,true);
	    $fname = $this->clean($fname,false);
	    $lname = $this->clean($lname,false);
	    $uid = $this->clean($uid,true);
	    $q1 = "UPDATE contact SET ctc_firstname='$fname',ctc_lastname='$lname' WHERE ctc_id=$cid AND ctc_usr_id_fk=$uid";
	    $this->update($q1);
	    if($numbers) {
    	    $this->deletePhoneNumbers($cid);
            $multiInsert = "";
            foreach($numbers as $phone) {
                $type = $this->clean($phone["type"],false);
                $number = $this->clean($phone["number"],true);
                $ext = $this->clean($phone["extension"],true);
                $multiInsert .= "(0,$number,$ext,'$type',$cid),";
            }
            $q2 = "INSERT INTO phone VALUES ".rtrim($multiInsert,",");
            $r2 = $this->insert($q2);
	    }
	    $this->close();
	    return $cid;
	}
	
	/*
	 * Returns all contact details, including phone numbers
	 * Left join is required because we allow the storage of contacts that have no phone numbers assigned yet.
	 * 
	 */
	function getContacts($uid) {
		$this->connect();
		$uid = $this->clean($uid,true);
        $q = "SELECT * FROM contact LEFT OUTER JOIN phone ON pho_ctc_id_fk=ctc_id WHERE ctc_usr_id_fk=$uid ORDER BY ctc_lastname ASC";
		$r = $this->select($q);
		$this->close();
		return $r;
	}
	
	/*
	 * Search by keywords
	 * TODO: Test REGEXP on multi-byte characters
	 * 
	 */
	function filterContacts($kw,$uid) {
		$this->connect();
		$uid = $this->clean($uid,true);
		$kw = $this->clean($kw,false);
		$re = str_replace(" ","|",trim($kw));
		$q = "SELECT * FROM contact LEFT OUTER JOIN phone ON pho_ctc_id_fk=ctc_id WHERE ctc_usr_id_fk=$uid AND (ctc_firstname REGEXP '$re' OR ctc_lastname REGEXP '$re') ORDER BY ctc_lastname ASC";
		$r = $this->select($q);
		$this->close();
		return $r;
	}
	
	function deleteContact($cid,$uid) {
		$this->connect();
		$uid = $this->clean($uid,true);
		$cid = $this->clean($cid,true);
		$q = "DELETE FROM contact WHERE ctc_id=$cid AND ctc_usr_id_fk=$uid";
		$r = $this->delete($q);
		$this->close();
		return $r;
	}
	
	/*
	 * Private method used by updateContact to delete all phone numbers associated with a contact.
	 * 
	 */
	private function deletePhoneNumbers($cid) {
	    $cid = $this->clean($cid,true);
	    $q = "DELETE FROM phone WHERE pho_ctc_id_fk=$cid";
	    $r = $this->delete($q);
	    return $r;
	}
}
?>