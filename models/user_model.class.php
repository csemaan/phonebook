<?php 
class UserModel extends Sql {
    
    /*
     * Get the password salt associated with a username.
     * 
     */
    private function getPwdSalt($uname) {
        $uname = $this->clean($uname,false);
        $q = "SELECT usr_pwdsalt FROM user WHERE usr_uname='$uname'"; 
        $r = $this->select($q);
        if($r) {
            return $r[0]["usr_pwdsalt"];
        }
        return false;
    }
    
    /*
     * Returns the user record corresponding to the given username and password
     * Compare username and password encrypting it with sha512+salt in PHP before
     * sending it on the wire to MySQL
     * 
     */
    function login($uname,$pwd) {
        $this->connect();
        $uname = $this->clean($uname,false);
        $pwd = $this->clean($pwd,false);
        $encryptedPwd = hash("sha512",$this->getPwdSalt($uname).$pwd);
        $q = "SELECT usr_id FROM user WHERE usr_uname='$uname' AND usr_pwd='$encryptedPwd'";
        $r = $this->select($q);
        $this->close();
        return $r;
    }
}
?>